/**函数模块 */

/**
 * @name: debounce
 * @cname: 防抖函数
 * @desc: 指定时间内内多次触发同一事件 只执行最后一次或最开始的一次 中间的不执行
 * @param {*} fn wait
 * @panme: fn=function () { console.log(1) } wait=5000
 * @result: 延迟五秒后打印1
*/
export function debounce(fn, wait) {
    let timer = null
    return function () {
        if (timer) clearTimeout(timer)
        timer = setTimeout(() => {
            fn.apply(this, arguments)
            timer = null
        }, wait);
    }
}

/**
 * @name: throttling
 * @cname: 节流函数
 * @desc: 指连续触发事件但是在 n 秒中只执行一次函数 控制出发频率
 * @param {*} fn wait
 * @panme:  fn = function () { console.log(1) } wait=5000
 * @result: 五秒内打印一次
*/
export function throttling(fn, wait) {
    let endTime = +new Date
    return function () {
        if (+new Date - endTime < wait) return console.log('too busy');
        fn.apply(this, arguments)
        endTime = +new Date
    }
}

/**
 * @name: compose
 * @cname: 复合函数
 * @desc: 将多个函数组合成一个函数，让后面的函数返回结果当成前面函数的参数
 * @param {...any} args 
 * @panme: a(b(c(10)))
 * @result: 当c函数返回结果 该结果会变成b的参数 运行后返回 再次变成a的参数
*/
export function compose(...args) {
    if (!args) throw 'args is required'
    return args.reduce((a, b) => params => a(b(params)))
}