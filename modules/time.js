/** 时间模块 */
import { repair0, last } from './util'

export let isDate = (obj) => toString(obj) === '[object Date]'

/**
 * @name: parseNumber2TimeArray
 * @cname: 转化秒数为数组
 * @desc: 将秒数转换为对应的时分秒数组
 * @param {*}  total
 * @panme: total=7200
 * @result: 7200s=>[2,0,0]
*/
export function parseNumber2TimeArray(total) {
	let hh = undefined,
		mm = undefined,
		ss = undefined

	total >= 3600 && (hh = Math.floor(total / 3600) < 10 ? Math.floor(total / 3600) : Math.floor(total / 3600))
	total >= 60 && (mm = Math.floor((total / 60 % 60)) < 10 ? Math.floor((total / 60 % 60)) : Math.floor((total /
		60 % 60)))
	total > 0 && (ss = Math.floor((total % 60)) < 10 ? Math.floor((total % 60)) : Math.floor((total % 60)));

	return [hh, mm, ss]
}

/**
 * @name: parseNumber2HMS
 * @cname: 转化为h:m:s
 * @desc: 将秒数转化为时间显示 h:m:s
 * @param {*}  total
 * @panme: total=7200
 * @result: 2:00:00
*/
export function parseNumber2HMS(total) {
	let arr = parseNumber2TimeArray(total)
	return arr.map(r => r || '00').map(repair0).join(':')
}

/**
 * @name: parseNumber2HMS_CN
 * @cname: 转化为h时m分s秒
 * @desc: 将秒数转化为时间显示 h时m分s秒
 * @param {*}  total
 * @panme: total=7200
 * @result: 2时00分00秒
*/
export function parseNumber2HMS_CN(total, full) {
	let [hh, mm, ss] = parseNumber2TimeArray(total)
	if (hh) hh += '时'
	if (mm) mm += '分'
	if (ss) ss += '秒'
	return [hh, mm, ss].join('')
}

export function convertMysqlTime2JSTime(val) {
	return new Date(new Date(val).toISOString().replace("T", ' ').split('.')[0])
}

/**
 * @name: formatMs2Obj
 * @cname: 将秒数转换
 * @desc: 将秒数转换成时分秒
 * @param {*}  total
 * @panme: total=2000
 * @result: {h: '00', m: '30', s: '00'}
*/
export function formatMs2Obj(total) {
	var h = repair0(Math.floor(total / 3600))
	var m = repair0(Math.floor((total - h * 3600) / 60))
	var s = repair0(Math.floor(total - h * 3600 - m * 60))
	return {
		h,
		m,
		s
	}
}

/**
 * @name: parseFullTime
 * @cname: 展示具体信息
 * @desc: 展示传入时间具体是年月日、时分秒、周几以及总秒数
 * @param {*}  val callback
 * @panme: val=new Date()
 * @result: {"yymmdd": "2021-12-13","hhmiss": "20:23:59","weekday": "星期一","ms": 1639398239040}
*/
export function parseFullTime(val, callback) {
	if (!val) return ''
	let d = new Date(val)
	let yy = d.getFullYear()
	let mm = repair0(d.getMonth() + 1)
	let dd = repair0(d.getDate())
	let hh = repair0(d.getHours())
	let mi = repair0(d.getMinutes())
	let ss = repair0(d.getSeconds())
	let yymmdd = [yy, mm, dd].join('-')
	let hhmiss = [hh, mi, ss].join(':')
	let weekday = `星期${'日一二三四五六'[d.getDay()]}`
	let ms = d.getTime()
	return callback({
		yymmdd, hhmiss, weekday, ms
	})
}

/**
 * @name: parseYMD
 * @cname: 展示年月日
 * @desc: 展示传入参数年月日
 * @param {*}  val
 * @panme: val=new Date()
 * @result: 2021-12-13
*/
export function parseYMD(val) {
	return parseFullTime(val, res => res.yymmdd)
}

/**
 * @name: parseHMS
 * @cname: 展示时分秒
 * @desc: 展示传入参数时分秒
 * @param {*}  val
 * @panme: val=new Date()
 * @result: 20:27:09
*/
export function parseHMS(val) {
	return parseFullTime(val, res => res.hhmiss)
}

/**
 * @name: parseWeek
 * @cname: 展示周几
 * @desc: 展示传入参数是周几
 * @param {*}  val
 * @panme: val=new Date()
 * @result: 星期一
*/
export function parseWeek(val) {
	return parseFullTime(val, res => res.weekday)
}

/**
 * @name: parseHMSWeek
 * @cname: 展示年月日和周几
 * @desc: 展示传入参数的年月日和周几
 * @param {*}  val
 * @panme: val=new Date()
 * @result: 2021-12-13 星期一
*/
export function parseHMSWeek(val) {
	return parseFullTime(val, res => res.yymmdd + ' ' + res.weekday)
}

/**
 * @name: parseHMSWeek
 * @cname: 展示总秒数
 * @desc: 展示传入参数的总秒数
 * @param {*}  val
 * @panme: val=new Date()
 * @result: 1639398542697
*/
export function parseMS(val) {
	return parseFullTime(val, res => res.ms)
}

/**
 * @name: parseHMSWeek
 * @cname: 展示日期
 * @desc: 展示传入参数的日期
 * @param {*}  val
 * @panme: val=new Date()
 * @result: 2021-12-13 20:29:36
*/
export function parseTime(date) {
	return parseFullTime(date, ({ yymmdd, hhmiss }) => [yymmdd, hhmiss].join(' '))
}


/**
 * @name: getFormatWeek
 * @cname: 获取星期几
 * @desc: 获取星期几
 * @param {*}  
 * @panme: getDay()
 * @result: 方法本返回0~6数字 改为返回日一二三四五六
*/
/**
 * 获取星期几文本
 */
export function getFormatWeek() {
	return "日一二三四五六"[getDay()]
}

/**
 * @name: getCurrentWeekDates
 * @cname: 某个日期的数组
 * @desc: 某个日期的一周内日期数组
 * @param {*}  date
 * @panme: date=new Date()
 * @result: 0: Mon Dec 06 2021 00:00:00 GMT+0800 (中国标准时间) {} 1: Tue Dec 07 2021 00:00:00 GMT+0800 (中国标准时间) {} ...
*/
export function getCurrentWeekDates(date) {
	let now = date ? new Date(date) : new Date()
	now.setHours(0)
	now.setMinutes(0)
	now.setSeconds(0)
	let wk = now.getDay() || 7
	now.setDate(now.getDate() - wk)
	let res = Array.from({ length: 7 }).map(() => new Date(now.setDate(now.getDate() + 1)))
	let sunDay = last(res)
	sunDay.setHours(23)
	sunDay.setMinutes(59)
	sunDay.setSeconds(59)
	return res
}

/**
 * @name: getCurrentWeekDatesYMD
 * @cname: 获取日期的年月日格式
 * @desc: 获取某个日期的一周内日期数组的年月日格式
 * @param {*}  date
 * @panme: date=new Date()
 * @result: ["2021-12-13","2021-12-14","2021-12-15","2021-12-16","2021-12-17","2021-12-18","2021-12-19"]
*/
export function getCurrentWeekDatesYMD(date) {
	return getCurrentWeekDates(date).map(parseYMD)
}

/**
 * @name: getCurrentWeekRange
 * @cname: 获取时间区间
 * @desc: 获取某个日期的周一0点到周日24点的区间 
 * @param {*}  date
 * @panme: date=new Date()
 * @result: end: Sun Dec 12 2021 23:59:59 GMT+0800 (中国标准时间) {} start: Mon Dec 06 2021 00:00:00 GMT+0800 (中国标准时间) {}
*/
export function getCurrentWeekRange(date) {
	let today = date ? new Date(date) : new Date()
	let wk = today.getDay() || 7
	let start = new Date()
	start.setDate(today.getDate() - wk + 1)
	start.setHours(0)
	start.setMinutes(0)
	start.setSeconds(0)
	let end = new Date()
	end.setDate(today.getDate() + (7 - wk))
	end.setHours(23)
	end.setMinutes(59)
	end.setSeconds(59)
	return { start, end }
}

/**
 * @name: getCurrentWeekRange2
 * @cname: 获取时间区间-代码简洁版
 * @desc: 获取某个日期的周一0点到周日24点的区间-时间换代码简洁 
 * @param {*}  date
 * @panme: date=new Date()
 * @result: end: Sun Dec 12 2021 23:59:59 GMT+0800 (中国标准时间) {} start: Mon Dec 06 2021 00:00:00 GMT+0800 (中国标准时间) {}
*/
export function getCurrentWeekRange2(date) {
	let [start, , , , , , end] = getCurrentWeekDates(date)
	return { start, end }
}

/**
 * 获取两个日期的天数间隔
 **/
export const dayDiff = (future, start) => {
	if (!future) throw 'future is required'
	if (!start) throw 'start is required'
	if (!isDate(start)) start = new Date(start)
	if (!isDate(future)) future = new Date(future)
	return Math.ceil(Math.abs(future.getTime() - start.getTime()) / 86400000)
}

/**
 * 获取某天属于今年的第几天
 *  	_.dayOfYear()
		_.dayOfYear(new Date())
		_.dayOfYear(new Date('2023-12-02 00:11:11'))
		_.dayOfYear(new Date('2023-12-02'))
		_.dayOfYear('2023-12-02')
 **/
export const dayOfYear = (date) => {
	if (!date) date = new Date()
	if (!isDate(date)) date = new Date(date)
	return Math.floor((date - new Date(date.getFullYear(), 0, 0)) / 1000 / 60 / 60 / 24);
}