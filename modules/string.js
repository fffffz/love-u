/** 字符串模块 */

/**
 * @name: isEmptyStr
 * @cname: 是否是空字符串
 * @desc: 判断是否为空字符串
 * @param {*}  str
 * @panme: str=''
 * @result: true
*/
export let isEmptyStr = (str) => !str || !str.trim()

/**
 * @name: limitStr
 * @cname: 截取字符串
 * @desc: 截取相应下标开始的字符串
 * @param {*}  str limit
 * @panme: str='花木成畦手自栽花木成畦手自栽' limit=2
 * @result: 花木...
*/
export function limitStr(str, limit = 20) {
	if (!str) return ''
	if (str.length <= limit) return str
	return str.substr(0, limit) + '...'
}

/**
 * @name: reverseStr
 * @cname: 字符串翻转
 * @desc: 反转整个字符串
 * @param {*}  str
 * @panme: str='花木成畦手自栽花木成畦手自栽'
 * @result: 栽自手畦成木花栽自手畦成木花
*/
export function reverseStr(str) {
	return str.split('').reverse().join('')
}

/**
 * @name: includes
 * @cname: 判断是否包含某字符串
 * @desc: 贩毒案字符串是否包含某个字符串
 * @param {*}  a b 
 * @panme: a='花木成畦手自栽花木成畦手自栽' b='木'
 * @result: ture
*/
export function includes(a, b) {
	return a.indexOf(b) > -1
}

/**
 * @name: startsWith
 * @cname: 判断是否以某字符串做开头
 * @desc: 判断字符串是否以某字符串做开头
 * @param {*}  a b
 * @panme: a='花木成畦手自栽花木成畦手自栽' b='花'
 * @result: 
*/
export function startsWith(a, b) {
	return a.indexOf(b) === 0
}

/**
 * @name: toNumber
 * @cname: 字母转换为数字
 * @desc: 转数字若字母则返回0，若中间有字母则返回至数字结尾
 * @param {*}  str 
 * @panme: str='54545ad454'
 * @result: 54545
*/
export function toNumber(str) {
	var result = parseFloat(str)
	return isNaN(result) ? 0 : result
}

/**
 * @name: toCapitalized
 * @cname: 首字母大写
 * @desc: 字符串的首字母大写
 * @param {*}  str
 * @panme: str='ad'
 * @result: Ad
*/
export function toCapitalized(str) {
	if (!str) return ''
	return str[0].toUpperCase() + str.substring(1)
}

/**
 * @name: mosaic
 * @cname: 以星号代替字符串
 * @desc: 某字符串从指定下标到指定下标以星号代替
 * @param {*}  str start end code
 * @panme: str='花木成畦手自栽花木成畦手自栽' start=0 end=5
 * @result: *****自栽花木成畦手自栽
*/
export function mosaic(str, start, end, code = "*") {
	if (!start && start !== 0) throw new Error("参数有误")
	if (isNaN(start) || start < 0 || start >= str.length)
		throw new Error("参数有误")
	if (!end || end >= str.length) end = str.length
	if (isNaN(end) || end <= start) throw new Error("参数有误")

	var left = str.substring(0, start)
	var middle = code.repeat(end - start)
	end = str.substring(end)
	return left + middle + end
}

/**
 * @name: checkStrNullOrEmpty
 * @cname: 判断是否为空
 * @desc: 判断字符串是否为空 空为true 否为flase
 * @param {*}  str
 * @panme: arr='1231
 * @result: flase
*/
export function checkStrNullOrEmpty(str) {
	str += ''//防止0或者false被误判断
	return !str || str.trim() === ''
}

/**
 * @name: hasVal
 * @cname:checkStrNullOrEmpty取反
 * @desc: 判断字符串是否为空 空为flase 否为true
 * @param {*}  str
 * @panme: str='1232321321
 * @result: true
*/
export function hasVal(str) {
	return !checkStrNullOrEmpty(str)
}

/**
 * @name: checkStrLength
 * @cname: 判断字符串长度
 * @desc: 判断字符串长度是否大于某个数字
 * @param {*}  str len
 * str='    花木成畦手自栽花木成畦手自栽' len='10'
 * @result: true
*/
export function checkStrLength(str, len) {
	return str && str.trim().length >= len
}

/**
 * 字符串内大小写互换   Hello=> hELLO
 * @param {*} str 
 * @returns 
 */
export function transLUCase(str) {
	if (!str) return ''
	//大小写互换
	return Array.from(str).map(r => /[A-Z]/.test(r) ? r.toLowerCase() : r.toUpperCase()).join('')
}

/**
 * 随机字符串
 * @returns str
 */
export let rndStr = () => Math.random().toString(36).slice(2);

/**
 * 判断是否是json字符串
 * @param {*} str 
 * @returns boolean
 * _.isJson('asdfaf') false
 * _.isJson('[1]') true
 */
export const isJson = str => {
	try {
		JSON.parse(str);
		return true;
	} catch (e) {
		return false;
	}
};